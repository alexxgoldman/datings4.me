import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import settings from "@/settings";

require('@/store/subscriber')

axios.defaults.baseURL = location.href.indexOf('.me') > 0
  ? `${settings.domain.prod}/api`
  : `${settings.domain.dev}/api`
console.log('axios.defaults.baseURL')
console.log(axios.defaults.baseURL)
// this url will automatically be attached to routes like `auth/signin`

Vue.config.productionTip = false

store.dispatch('auth/attempt', localStorage.getItem('token')).then(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})